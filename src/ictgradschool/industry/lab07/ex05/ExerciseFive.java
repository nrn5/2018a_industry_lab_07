package ictgradschool.industry.lab07.ex05;

import ictgradschool.Keyboard;

import javax.naming.InitialContext;
import java.security.PrivateKey;

/**
 * TODO Write a program according to the Exercise Five guidelines in your lab handout.
 */
public class ExerciseFive {

    public void start() {
        // TODO Write the codes :)
        try {
            String stringOfCharacters = getStringOfCharactersFromUser();
            String initials = getInitialsFromWord(stringOfCharacters);
            System.out.println("You entered: " + initials);
        } catch (InvalidWordException e) {
            System.out.println("The first character of your word must be a letter");
        } catch (ExceedMaxStringLengthException e) {
            System.out.println("Your string was too long. Go away and think about what you've done");
        }
    }


    public class InvalidWordException extends Exception {
        public InvalidWordException() {
        }

        public InvalidWordException(String message) {
            super(message);
        }

        public InvalidWordException(String message, Throwable cause) {
            super(message, cause);
        }

        public InvalidWordException(Throwable cause) {
            super(cause);
        }
    }

    public class ExceedMaxStringLengthException extends Exception {
        public ExceedMaxStringLengthException() {
        }

        public ExceedMaxStringLengthException(String message) {
            super(message);
        }

        public ExceedMaxStringLengthException(String message, Throwable cause) {
            super(message, cause);
        }

        public ExceedMaxStringLengthException(Throwable cause) {
            super(cause);
        }
    }


    // TODO Write some methods to help you.
    private String getStringOfCharactersFromUser() throws ExceedMaxStringLengthException {
        System.out.println("Enter a string of at most 100 characters: ");
        String stringOfCharacters;

        stringOfCharacters = Keyboard.readInput();

        if (stringOfCharacters.length() > 100) {
            throw new ExceedMaxStringLengthException("String was too long");
        }

        return stringOfCharacters;
    }

    private String getInitialsFromWord(String stringOfCharacters) throws InvalidWordException {

        String[] words = stringOfCharacters.split(" ");
        String initials = "";

        for (int i = 0; i < words.length; i++) {
            if (words[i].matches("\\d.*")) {
                throw new InvalidWordException("First character not a letter");
            }

            initials += words[i].charAt(0) + " ";
        }

        return initials;
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        new ExerciseFive().start();
    }
}
